## Problem
Given a square matrix, calculate the absolute 
difference between the sums of its diagonals.

## Function description
It must return an integer 
representing the absolute diagonal difference.

## Input Format
The first line contains a single integer, 
**n**, the number of rows and columns in the matrix **arr**.  
Each of the next **n** lines describes a row, 
**arr[i]**, and consists of **n** space-separated integers **arr[i][j]**.

## Constraints
**-100 <= arr[i][j] <= 100**

## Output Format
Print the absolute difference between 
the sums of the matrix's two diagonals as a single integer.

## Sample Input
3  
11 2 4  
4 5 6  
10 8 -12  

## Sample Output
15