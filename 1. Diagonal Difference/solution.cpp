#include <iostream>
#include <vector>

using namespace std;

int diagonal_difference(const vector<vector<int>> &array) {
    int sum = 0;

    int size = array.size();
    for (int i = 0; i < size; i++) {
        sum += array[i][i] - array[i][size - 1 - i];
    }

    return sum > 0 ? sum : -sum;
}

int main() {
    int n;
    cin >> n;

    vector<vector<int>> array(n);
    for (int i = 0; i < n; i++) {
        array[i].resize(n);
        for (int j = 0; j < n; j++) {
            cin >> array[i][j];
        }
    }

    cout << diagonal_difference(array);

    return 0;
}