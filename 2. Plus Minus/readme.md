## Problem
Given an array of integers, calculate the fractions of its elements 
that are positive, negative, and are zeros. 
Print the decimal value of each fraction on a new line.

**Note:** This challenge introduces precision problems. 
The test cases are scaled to six decimal places, 
though answers with absolute error of up to 10-4 are acceptable.

## Function Description
It should print out the ratio of 
positive, negative and zero items in the array, 
each on a separate line rounded to six decimals.

## Input Format
The first line contains an integer, **n**, denoting the size of the array.
The second line contains **n** space-separated integers describing an array of numbers.

Constraints
**0 < n <= 100**
**-100 <= arr[i] <= 100**

## Output Format
You must print the following **3** lines:
A decimal representing of the fraction of 
positive numbers in the array compared to its size.
A decimal representing of the fraction of 
negative numbers in the array compared to its size.
A decimal representing of the fraction of 
zeros in the array compared to its size.

## Sample Input
6  
-4 3 -9 0 4 1         

## Sample Output
0.500000  
0.333333  
0.166667