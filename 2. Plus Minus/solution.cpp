#include <vector>
#include "iostream"

using namespace std;

float *plus_minus(float *result) {
    int size;
    cin >> size;

    int number;
    int pos = 0, neg = 0, zero = 0;

    for (int i = 0; i < size; i++) {
        cin >> number;

        if (number > 0) {
            pos++;
        } else if (number < 0) {
            neg++;
        } else {
            zero++;
        }
    }

    result[0] = (float) pos / (float) size;
    result[1] = (float) neg / (float) size;
    result[2] = (float) zero / (float) size;

    return result;
}

int main() {
    float result[3];

    plus_minus(result);

    for (float i : result) {
        printf("%.6f\n", i);
    }

    return 0;
}